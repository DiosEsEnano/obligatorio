; (function (global) {
  var AddressBook = function (titulo, autor, isbn, tema, stock, precio) {
    return new AddressBook.init(titulo, autor, isbn, tema, stock, precio);
  };

  AddressBook.prototype = {
    //default functions
    book: [
      //add book here
    ],
    searchResults: [

    ],
    addNewBook: function (titulo, autor, isbn, tema, stock, precio) {

      this.book.push({

        titulo: titulo,
        autor: autor,
        isbn: isbn,
        tema: tema,
        stock: stock,
        precio: precio,
     
      
      }),
    }
    save: function () {
      //save to local storage. This isn't hugely necessary

    },
    returnAll: function () {
      return this.book;
    },
    displayBook: function () {
      this.log(this.book);
      return this;
    },
    log: function (book) {
      console.log(book);
      return this;
    },
    search: function (searchTerm) {
      if (this.book.length) {
        for (var i = 0; i < this.book.length; i++) {
          if (this.book[i].titulo.toLowerCase() == searchTerm.toLowerCase()) {
            console.error(this.book[i]);
            this.searchResults.push(this.book[i]);
          }
        }

        return this.searchResults;
      } else {
        console.log("No hay resultados");
      }
      return this;
    },
    lastResults: function () {
      return this.searchResults;
    }
  }

  AddressBook.init = function (titulo, autor, isbn, tema, stock, precio) {
    var self = this;
    //set up the address book
    if (titulo || autor || isbn || tema || stock || precio) {
      self.addNewBook(titulo || "", autor || "", isbn || "", tema || "", stock || "", precio || "");
    }

  }

  AddressBook.init.prototype = AddressBook.prototype;

  global.AddressBook = $ab = AddressBook;
})(window);

if (!window.bookList) { //check if we already have a book list
  window.bookList = $ab();
}

var form = document.getElementById('book');
form.addEventListener('submit', function () {
  if (!window.bookList) { //check if we already have a book list
    window.bookList = $ab(form.titulo.value, form.autor.value, form.isbn.value, form.tema.value, form.stock.value, form.precio.value);
  } else {
    //saves new values rather than deleting old ones as well
    bookList.addNewBook(form.titulo.value, form.autor.value, form.isbn.value, form.tema.value, form.stock.value, form.precio.value);
  }

  form.titulo.value = '';
  form.autor.value = '';
  form.isbn.value = '';
  form.tema.value = '';
  form.stock.value = '';
  form.precio.value = '';

  event.preventDefault();
});

var searchForm = document.getElementById('search');
searchForm.addEventListener('submit', function () {
  var results;
  if (results !== undefined) {
    results = null;
  }
  if (!window.bookList) {
    window.bookList = $ab();
  } else {
    results = bookList.search(searchForm.search.value);
  }
  document.getElementById('results').innerHTML = '';
  if (results.length > 0) {

    for (var i = 0; i < results.length; i++) {
      document.getElementById('results').innerHTML += '<div class="book-item">Titulo:' + book[i].titulo + '<br>Autor:' + book[i].autor + '<br>ISBN:' + book[i].isbn + '<br>tema:' + book[i].tema + '<br>stock:' + book[i].stock + '<br>Precio:' + book[i].precio + '</div><hr>';
    }
  } else {
    document.getElementById('results').innerHTML += '<div class="book-item">There are no results for this name</div><hr>';
  }

  //do something with the results
  event.preventDefault();
});

document.getElementById('js-show-all').addEventListener('click', function () {
  if (window.bookList) { //check if we already have a book list
    document.getElementById('show-panel').innerHTML = '';
    var book = bookList.returnAll();
    console.log(book);
    if (book.length > 0) {
      for (var i = 0; i < book.length; i++) {
        document.getElementById('show-panel').innerHTML += '<div class="book-item">Titulo:' + book[i].titulo + '<br>Autor:' + book[i].autor + '<br>ISBN:' + book[i].isbn + '<br>tema:' + book[i].tema + '<br>stock:' + book[i].stock + '<br>Precio:' + book[i].precio + '</div><hr>';
      }
    } else {
      document.getElementById('show-panel').innerHTML += '<div class="book-item">No tienes ningún libro ¿Por qué no agregas uno? </div><hr>';
    }
  }
  document.getElementById('show-panel').style.display = 'block';

  document.getElementById('search-panel').style.display = 'none';
  document.getElementById('book-panel').style.display = 'none';
});

document.getElementById('js-search').addEventListener('click', function () {
  document.getElementById('show-panel').style.display = 'none';
  document.getElementById('search-panel').style.display = 'block';
  document.getElementById('book-panel').style.display = 'none';
});

document.getElementById('js-add-new').addEventListener('click', function () {
  document.getElementById('show-panel').style.display = 'none';
  document.getElementById('search-panel').style.display = 'none';
  document.getElementById('book-panel').style.display = 'block';
});